# Auth APIs
  1. Auth APIs: https://hexdocs.pm/access_pass/phoenix_routes_helper.html#content
  1. Change password for authenticated user 
      ```
      POST `/change_password`
      ```
      This endpoint is used to change password for authenticated users. The flow is as follows.

      Request password reset

      |> On form submit send a new password and password confirm to this endpoint.

      Email is sent to user informing them about a password change.

      Required Params
      
      body: 
      ```
      { 
        "password_confirm": "the new password to be set for a user",
        "new_password": "the new password to be set for a user"
      }
      ```
## 2FA APIs
  1. Determine if 2FA is enabled
      ```
      GET /check
      ```

      Required Params
      * headers: access-token

      Returns
      ```json
      {
        "ok": {
          "email":"jane@example.com",
          "email_confirmed":false,
          "id":1,
          "meta": {},
          "enable_2fa":true,
          "username":"jane"
        }
      }
      ```
  1. Generate TOTP
      ```
      POST /generate_totp_url
      ```
      This endpoint is used to generate the TOTP URI.

      Required Params
      * headers: access-token

      Returns
      ```json
      {"ok": "otpauth://totp/Acme:alice?secret=MFRGGZA&issuer=Acme"}
      ```
  1. Enable 2FA
      ```
      POST /enable_2fa
      ``` 
      This endpoint is used to enable 2FA.

      Required Params
      * headers: access-token
      ```json
      {
        "code": "given code"
      }
      ```

      Returns
      ```json
      {"ok": "two factor auth enabled"}
      ```
  1. Disable 2FA
      ```
      POST /disable_2fa
      ```
      This endpoint is used to disable 2FA.

      Required Params
      * headers: access-token

      Returns
      ```json
      {"ok": "two factor auth disabled"}
      ```
  1. Validate TOTP
      ```
      POST /check_totp
      ```
      This endpoint is used to validate TOTP code or recovery code before returning tokens.

      Required Params
      ```json
      { 
        "username": "username",
        "password": "password",
        "code": "123456"
      }
      ```
      Response on valid creds.
      ```json
      {
        "ok": {
          "type":"basic",
          "refresh_token":"MjNmYzgzNGMtMGM3MS00YTA4LTkxMWMtNDEyODU3Yzk2ZTgy",
          "refresh_expire_in":1200,
          "access_token":"ODhhMDgzYjctZTE3OC00YjgyLWFiZGMtZTJjOWZiMzJjODhi",
          "access_expire_in":600
        }
      }
      ```
      Response on invalid creds.
      ```json
      {
        "ok": {
          
        }
      }
      ```
  1. Generate Recovery Codes
      ```
      POST /generate_recovery_codes
      ```
      This endpoint is used to generate recovery codes.

      Required Params
      * headers: access-token

      Returns
      ```json
      {
        "ok": [
          "099345", 
          "076698", 
          "115899", 
          "327675", 
          "951647", 
          "191725", 
          "333458", 
          "715267",
          "565345", 
          "010398"
        ]
      }
      ```