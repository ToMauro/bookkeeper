export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Target: https://go.nuxtjs.dev/config-target
  target: "static",

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "bookkeeper-ui",
    htmlAttrs: {
      lang: "en",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["@/assets/css/main.css", "spinkit/spinkit.css"],

  loading: false,

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    "~/plugins/axios",
    "~/plugins/vee-validate",
    "~/plugins/vue-js-modal",
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://github.com/nuxt/postcss8
    "@nuxt/postcss8",
    // https://go.nuxtjs.dev/typescript
    "@nuxt/typescript-build",
    // https://composition-api.nuxtjs.org
    "@nuxtjs/composition-api/module",
    // https://go.nuxtjs.dev/stylelint
    "@nuxtjs/stylelint-module",
    // https://github.com/nuxt-community/svg-module#nuxtjssvg
    "@nuxtjs/svg",
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    "@nuxtjs/axios",
    // https://dev.auth.nuxtjs.org
    "@nuxtjs/auth-next",
    // https://github.com/f/vue-wait#%EF%B8%8F-usage-with-nuxtjs
    "vue-wait/nuxt",
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    postcss: {
      plugins: {
        tailwindcss: {},
        autoprefixer: {},
      },
    },
  },

  // Env Configuration: https://nuxtjs.org/docs/configuration-glossary/configuration-env
  env: {},

  // Runtime Configuration: https://nuxtjs.org/docs/configuration-glossary/configuration-runtime-config/
  publicRuntimeConfig: {
    unauthorizedCode: process.env.UNAUTHORIZED_CODE ?? 400,

    changePasswordUrl:
      process.env.AUTH_CHANGE_PASSWORD_URL ?? "/change_password",
    confirmUrl: process.env.AUTH_CONFIRM_URL ?? "/confirm",
    forgotUsernameUrl:
      process.env.AUTH_FORGOT_USERNAME_URL ?? "/forgot_username ",
    registerUrl: process.env.AUTH_REGISTER_URL ?? "/register",
    resetPasswordUrl: process.env.AUTH_RESET_PASSWORD_URL ?? "/reset_password ",
  },

  // Auth module configuration: https://auth.nuxtjs.org/schemes/refresh#options
  auth: {
    strategies: {
      localRefresh: {
        scheme: "refresh",
        endpoints: {
          login: {
            url: process.env.AUTH_LOGIN_URL ?? "/login",
            method: "post",
          },
          logout: {
            url: process.env.AUTH_REGISTER_URL ?? "/logout",
            method: "post",
          },
          refresh: {
            url: process.env.AUTH_REFRESH_URL ?? "/refresh",
            method: "get",
          },
          user: {
            url: process.env.AUTH_USER_URL ?? "/check",
            method: "get",
          },
        },
        token: {
          property: "ok.access_token",
          maxAge: 300,
          name: "access-token",
          type: "",
        },
        refreshToken: {
          property: "ok.refresh_token",
          maxAge: false,
        },
        user: {
          property: "ok",
          autoFetch: true,
        },
      },
    },
    redirect: {
      login: "/login",
      logout: "/",
      home: "/dashboard",
    },
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: process.env.BASE_URL ?? "http://localhost:4000",
  },

  // Nuxt router configuration: https://github.com/nuxt-community/router-module
  router: {
    // Auth Middleware: https://auth.nuxtjs.org/guide/middleware
    middleware: ["auth"],
  },

  // Storybook Configuration: https://storybook.nuxtjs.org/api/options
  storybook: {
    port: 6006,
    addons: ["@storybook/addon-interactions", "storybook-addon-pseudo-states"],
    features: {
      interactionsDebugger: true,
    },
  },

  // Stylelint Configuration: https://github.com/nuxt-community/stylelint-module#options
  stylelint: {},

  // Vuewait Configuration: https://github.com/f/vue-wait#-vuewait-options
  wait: {
    useVuex: true,
  },
};
