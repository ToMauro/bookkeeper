import { computed, ComputedRef } from "@nuxtjs/composition-api";
import type { Ref } from "@nuxtjs/composition-api";

interface NestedError {
  [key: string]: string[];
}

export default function useErrorMsg(error: Ref<any>) {
  const errorMsg: ComputedRef<string> = computed(() => {
    try {
      const errorData: NestedError | string =
        error.value?.response?.data?.error;

      if (typeof errorData === "object") {
        const keys = Object.keys(errorData);
        const key =
          keys.find((key) => {
            return errorData[key]?.[0];
          }) ?? "";

        const [msg] = errorData[key];
        return `${key?.charAt(0).toUpperCase()}${key?.slice(1)} ${msg ?? ""}.`;
      }

      return "We were unable to process your request, please try again.";
    } catch (e) {
      return "Unknown error, please try again.";
    }
  });

  return {
    errorMsg,
  };
}
