import { computed, ComputedRef } from "@nuxtjs/composition-api";
import type { Ref } from "@nuxtjs/composition-api";

export enum SeverityLevel {
  default = "default",
  error = "error",
  info = "info",
  success = "success",
  warning = "warning",
}

export function useSeverityLevel(level: Ref<SeverityLevel>) {
  const iconNameBySeverityLevel: ComputedRef<String> = computed(() => {
    switch (level.value) {
      case SeverityLevel.error:
        return "XCircle";
      case SeverityLevel.info:
        return "InformationCircle";
      case SeverityLevel.success:
        return "CheckCircle";
      case SeverityLevel.warning:
        return "ExclamationTriangle";
      default:
        return "ExclamationCircle";
    }
  });

  const stylesBySeverityLevel: ComputedRef<SeverityLevel> = computed(
    () => SeverityLevel[level.value]
  );

  return {
    iconNameBySeverityLevel,
    stylesBySeverityLevel,
  };
}
