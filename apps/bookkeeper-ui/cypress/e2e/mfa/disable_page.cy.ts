describe("Mfa disable page", () => {
  beforeEach(() => {
    cy.setAsLoggedIn(true);
    cy.visit("/mfa");
  });

  it("successfully loads", () => {
    cy.findByText("Multi-factor Authentication");
  });

  it("displays confirmation modal", () => {
    cy.findByTestId("submit").click();
    cy.findByText("Confirmation for disabling two-factor authentication");
  });

  it("handles submission error", () => {
    cy.stubDisable2faApi({
      response: {
        statusCode: 400,
      },
    });
    cy.findByTestId("submit").click();
    cy.findByTestId("confirmBtn").click();
    cy.wait("@stubDisable2faApi");
    cy.findByText("Error disabling 2FA, please try again.");
  });

  it("switch to enable page after confirming", () => {
    cy.stubDisable2faApi();
    cy.stubCheckApi();
    cy.findByTestId("submit").click();
    cy.findByTestId("confirmBtn").click();

    cy.wait("@stubDisable2faApi");
    cy.wait("@stubCheckApi");
    cy.findByText("Register Two-Factor Authenticator");
  });
});
