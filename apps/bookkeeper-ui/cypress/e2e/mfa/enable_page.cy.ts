describe("Mfa enable page", () => {
  const urlStr = "otpauth://totp/Acme:alice?secret=TESTFAKE&issuer=Acme";

  beforeEach(() => {
    cy.setAsLoggedIn();
    cy.stubTotpUrlApi({
      response: {
        body: {
          ok: urlStr,
        },
      },
    });
    cy.visit("/mfa");
    cy.wait("@stubTotpUrlApi");
  });

  it("successfully loads", () => {
    cy.findByText("Register Two-Factor Authenticator");
    cy.findByTestId("submit").should("be.disabled");

    const url = new URL(urlStr);
    cy.findAllByText(`issuer: ${url.searchParams.get("issuer")}`);

    const secrets: string[] =
      url.searchParams.get("secret")?.match(/.{4}/g) ?? [];
    secrets.forEach((i) => cy.findAllByText(i));
  });

  it("handles numeric input error", () => {
    cy.findByTestId("code").type("fake").blur();
    cy.findByTestId("submit").should("be.disabled");
    cy.findByText(
      "Code needs to be numeric and have a length of 6 characters."
    );
  });

  it("handles submission error", () => {
    cy.stubEnable2faApi({
      response: {
        statusCode: 400,
        body: {
          error: "invalid code",
        },
      },
    });

    const code = "123456";
    cy.findByTestId("code").type(code);
    cy.findByTestId("submit").should("not.be.disabled").click();

    cy.wait("@stubEnable2faApi")
      .its("request.body")
      .should("deep.equal", { code });
    cy.findByText("Invalid two-factor authentication code.");
  });

  it("handles submission error", () => {
    cy.stubEnable2faApi();
    cy.findByTestId("code").type("123456");
    cy.findByTestId("submit").click();

    cy.stubRecoveryCodeApi({
      response: {
        statusCode: 400,
      },
    });
    cy.wait("@stubEnable2faApi");
    cy.wait("@stubRecoveryCodeApi");
    cy.findAllByText(
      "We're experiencing issues getting your recovery codes. Please reload the page."
    );
  });

  it("display recovery codes after form submission", () => {
    cy.stubEnable2faApi();
    cy.stubRecoveryCodeApi();
    cy.findByTestId("code").type("123456");
    cy.findByTestId("submit").click();
    cy.findByTestId("code").invoke("val").should("be.empty");

    cy.wait("@stubEnable2faApi");
    cy.wait("@stubRecoveryCodeApi");
    cy.findByTestId("recoveryCodeModal").should("be.visible");
  });

  it("switch to disable page after recovery code modal", () => {
    cy.stubEnable2faApi();
    cy.stubRecoveryCodeApi();
    cy.findByTestId("code").type("123456");
    cy.findByTestId("submit").click();

    cy.wait("@stubEnable2faApi");
    cy.wait("@stubRecoveryCodeApi");

    cy.stubCheckApi({
      response: {
        body: {
          ok: {
            username: "testuser123",
            enable_2fa: true,
            meta: {},
            id: 1,
            email_confirmed: true,
            email: "testuser123@gmail.com",
          },
        },
      },
    });
    cy.findByTestId("closeBtn").click();
    cy.wait("@stubCheckApi");
    cy.findByText("Multi-factor Authentication");
  });
});
