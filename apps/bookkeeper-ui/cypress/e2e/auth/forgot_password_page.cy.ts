describe("Forgot Password page", () => {
  it("successfully loads", () => {
    cy.visit("/forgot-password");
    cy.findByText("Reset Your Password");
    cy.findByTestId("submit").should("be.disabled");
  });

  it("redirects authenticated user to dashboard", () => {
    cy.setAsLoggedIn();
    cy.visit("/forgot-password");
    cy.url().should("contain", "/dashboard");
  });

  it("displays confirmation message", () => {
    cy.intercept(
      {
        method: "POST",
        url: "/reset_password",
      },
      { ok: "password reset sent to accounts email" }
    ).as("resetPasswordApi");

    cy.visit("/forgot-password");

    const params: { [key: string]: string } = {
      username: "fake1",
    };

    cy.findByTestId("username").type(params.username);
    cy.findByTestId("submit").should("not.be.disabled").click();

    cy.wait("@resetPasswordApi")
      .its("request.body")
      .should("deep.equal", params);
    cy.findByText(
      "An email with password reset information has been sent to you."
    );
  });
});
