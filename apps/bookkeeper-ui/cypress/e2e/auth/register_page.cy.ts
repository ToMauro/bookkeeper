describe("Register page", () => {
  it("successfully loads", () => {
    cy.visit("/register");
    cy.findByText("Register for an account");
    cy.findByTestId("submit").should("be.disabled");
  });

  it("redirects authenticated user to dashboard", () => {
    cy.setAsLoggedIn();
    cy.visit("/register");
    cy.url().should("contain", "/dashboard");
  });

  it.only("redirects user to dashboard on form submission", () => {
    cy.visit("/register");
    cy.stubCheckApi();
    cy.intercept(
      {
        method: "POST",
        url: "/register",
      },
      {
        ok: {
          type: "basic",
          refresh_token: "refresh_token",
          refresh_expire_in: 0,
          access_token: "access_token",
          access_expire_in: 300,
        },
      }
    ).as("registerApi");

    const params: { [key: string]: string } = {
      username: "test_username",
      email: "email@gmail.com",
      password: "Password12!",
      password_confirm: "Password12!",
    };
    for (const key in params) {
      cy.findByTestId(key).type(params[key]);
    }
    cy.findByTestId("submit").should("not.be.disabled").click();

    cy.wait("@registerApi").its("request.body").should("deep.equal", params);
    cy.wait("@stubCheckApi");

    cy.url().should("contain", "/dashboard");
  });

  const tests = [
    {
      key: "username",
      body: {
        error: {
          username: ["has already been taken"],
        },
      },
      expected: "Username has already been taken.",
    },
    {
      key: "email",
      body: {
        error: {
          email: ["has already been taken"],
        },
      },
      expected: "Email has already been taken.",
    },
  ];
  tests.forEach(({ key, body, expected }) => {
    it(`displays duplicate ${key} error`, () => {
      cy.visit("/register");
      cy.intercept(
        {
          method: "POST",
          url: "/register",
        },
        {
          statusCode: 400,
          body,
        }
      ).as("duplicateError");

      const params: { [key: string]: string } = {
        username: "test_username",
        email: "email@gmail.com",
        password: "Password12!",
        password_confirm: "Password12!",
      };

      for (const key in params) {
        cy.findByTestId(key).type(params[key]);
      }
      cy.findByTestId("submit").click();

      cy.wait("@duplicateError");
      cy.findByText(expected);
    });
  });
});
