describe("Confirm Account page", () => {
  it("successfully loads", () => {
    cy.visit("/confirm/fake-confirm-id");
    cy.findByText("Account Confirmation");
  });

  const tests: {
    name: string;
    res: {
      [key: string]: any;
    };
    msg: string;
    isAuth: boolean;
  }[] = [
    {
      name: "successfully confirm email",
      res: {
        ok: "email confirmed",
      },
      msg: "You've successfully confirmed your email. Go to login.",
      isAuth: false,
    },
    {
      name: "successfully confirm email while authenticated",
      res: {
        ok: "email confirmed",
      },
      msg: "You've successfully confirmed your email. Go to dashboard.",
      isAuth: true,
    },
    {
      name: "error confirming email",
      res: {
        statusCode: 400,
        body: {
          error: "email confirmation failed",
        },
      },
      msg: "There were issues confirming your account. Go to login.",
      isAuth: false,
    },
    {
      name: "error confirming email while authenticated",
      res: {
        statusCode: 400,
        body: {
          error: "email confirmation failed",
        },
      },
      msg: "There were issues confirming your account. Go to dashboard.",
      isAuth: true,
    },
  ];
  tests.forEach(({ name, res, msg, isAuth }) => {
    it(name, () => {
      cy.intercept(
        {
          method: "POST",
          url: "/confirm",
        },
        res
      ).as("confirmApi");

      if (isAuth) {
        cy.setAsLoggedIn();
      }
      cy.visit("/confirm/fake-confirm-id");

      cy.wait("@confirmApi");
      cy.contains(msg);
    });
  });

  [
    {
      name: "redirects to login page when confirm_id is missing",
      url: "/login",
      isAuth: false,
    },
    {
      name: "redirects to dashboard page when confirm_id is missing",
      url: "/dashboard",
      isAuth: true,
    },
  ].forEach(({ name, url, isAuth }) => {
    it(name, () => {
      if (isAuth) {
        cy.setAsLoggedIn();
      }

      cy.visit("/confirm");
      cy.url().should("contain", url);
    });
  });
});
