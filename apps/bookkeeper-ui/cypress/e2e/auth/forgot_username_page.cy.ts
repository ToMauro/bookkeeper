describe("Forgot Username page", () => {
  it("successfully loads", () => {
    cy.visit("/forgot-username");
    cy.findByText("Recover Your Username");
    cy.findByTestId("submit").should("be.disabled");
  });

  it("redirects authenticated user to dashboard", () => {
    cy.setAsLoggedIn();
    cy.visit("/forgot-username");
    cy.url().should("contain", "/dashboard");
  });

  it("displays confirmation message", () => {
    cy.intercept(
      {
        method: "POST",
        url: "/forgot_username",
      },
      { ok: "sent email with related username" }
    ).as("forgotUsernameApi");

    cy.visit("/forgot-username");

    const params: { [key: string]: string } = {
      email: "fake@email.com",
    };

    cy.findByTestId("email").type(params.email);
    cy.findByTestId("submit").should("not.be.disabled").click();

    cy.wait("@forgotUsernameApi")
      .its("request.body")
      .should("deep.equal", params);
    cy.findByText("An email has been sent to you with your username.");
  });
});
