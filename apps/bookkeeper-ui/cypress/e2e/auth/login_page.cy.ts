describe("Login page", () => {
  it("successfully loads", () => {
    cy.visit("/login");
    cy.findByText("Please login to your account");
    cy.findByTestId("submit").should("be.disabled");
  });

  it("redirects authenticated user to dashboard", () => {
    cy.setAsLoggedIn();
    cy.visit("/login");
    cy.url().should("contain", "/dashboard");
  });

  it("redirects user to dashboard on form submission", () => {
    cy.visit("/login");
    cy.stubCheckApi();
    cy.intercept(
      {
        method: "POST",
        url: "/login",
      },
      {
        ok: {
          type: "basic",
          refresh_token: "refresh_token",
          refresh_expire_in: 0,
          access_token: "access_token",
          access_expire_in: 300,
        },
      }
    ).as("loginApi");

    const params: { [key: string]: string } = {
      username: "test_username",
      password: "Password12!",
    };
    for (const key in params) {
      cy.findByTestId(key).type(params[key]);
    }
    cy.findByTestId("submit").should("not.be.disabled").click();

    cy.wait("@loginApi").its("request.body").should("deep.equal", params);
    cy.wait("@stubCheckApi");
    cy.url().should("contain", "/dashboard");
  });

  it("handles invalid credentials error", () => {
    cy.visit("/login");
    cy.intercept(
      {
        method: "POST",
        url: "/login",
      },
      {
        statusCode: 400,
        body: { error: "username or password is incorrect" },
      }
    ).as("loginApi");

    const params: { [key: string]: string } = {
      username: "test_username",
      password: "Password12!",
    };
    for (const key in params) {
      cy.findByTestId(key).type(params[key]);
    }

    cy.findByTestId("submit").click();
    cy.wait("@loginApi");
    cy.findByText(
      "The username and password you specified are invalid. Please try again."
    );
  });

  it("handles unexpected errors", () => {
    cy.visit("/login");
    cy.intercept(
      {
        method: "POST",
        url: "/login",
      },
      {
        statusCode: 401,
        body: { error: "username or password is incorrect" },
      }
    ).as("loginApi");

    const params: { [key: string]: string } = {
      username: "test_username",
      password: "Password12!",
    };
    for (const key in params) {
      cy.findByTestId(key).type(params[key]);
    }

    cy.findByTestId("submit").click();
    cy.wait("@loginApi");
    cy.findByText("Unexpected error. Please try again.");
  });

  describe("two-factor auth", () => {
    beforeEach(() => {
      cy.visit("/login");
      cy.stubCheckApi();
      cy.intercept(
        {
          method: "POST",
          url: "/login",
        },
        {
          ok: {
            two_factor_auth: true,
          },
        }
      ).as("loginApi");

      const params: { [key: string]: string } = {
        username: "test_username",
        password: "Password12!",
      };
      for (const key in params) {
        cy.findByTestId(key).type(params[key]);
      }
      cy.findByTestId("submit").click();

      cy.wait("@loginApi").its("request.body").should("deep.equal", params);
    });

    it("displays 2fa form", () => {
      cy.findByText("Two-factor authentication code");
      cy.findByTestId("submit").should("be.disabled");
    });

    it("handles numeric input error", () => {
      cy.findByTestId("code").type("fake").blur();
      cy.findByTestId("submit").should("be.disabled");
      cy.findByText(
        "Code needs to be numeric and have a length of 6 characters."
      );

      cy.findByTestId("code").clear();
      cy.findByTestId("code").type("123").blur();
      cy.findByTestId("submit").should("be.disabled");
      cy.findByText(
        "Code needs to be numeric and have a length of 6 characters."
      );
    });

    it("handles submission error", () => {
      cy.stubCheckTotpApi({
        response: {
          statusCode: 400,
          body: { error: "username or password is incorrect" },
        },
      });
      cy.findByTestId("code").type("123456").blur();
      cy.findByTestId("submit").should("not.be.disabled").click();

      cy.wait("@totpApi");
      cy.findAllByText("Invalid two-factor authentication code.");
    });

    it("redirect to dashboard after login", () => {
      cy.stubCheckTotpApi();
      cy.stubCheckApi({
        response: {
          body: {
            ok: {
              username: "testuser123",
              enable_2fa: true,
              meta: {},
              id: 1,
              email_confirmed: true,
              email: "testuser123@gmail.com",
            },
          },
        },
      });
      cy.findByTestId("code").type("123456").blur();
      cy.findByTestId("submit").should("not.be.disabled").click();

      cy.wait("@totpApi");
      cy.wait("@stubCheckApi");
      cy.url().should("contain", "/dashboard");
    });
  });
});
