describe("Password Reset page for unauthenticated user", () => {
  it("successfully loads", () => {
    cy.visit("/password-reset/fake-password-id");
    cy.findByText("Change Password");
    cy.findByTestId("submit").should("be.disabled");
  });

  it("redirects to login when missing password_id", () => {
    cy.visit("/password-reset");
    cy.url().should("contain", "/login");
  });

  it("displays confirmation message after submitting form", () => {
    cy.intercept(
      {
        method: "POST",
        url: "/change_password",
      },
      {
        ok: {
          type: "basic",
          refresh_token: "refresh_token",
          refresh_expire_in: 0,
          access_token: "access_token",
          access_expire_in: 300,
        },
      }
    ).as("changePwApi");

    const params: { [key: string]: string } = {
      password_id: "fake-password-id",
      new_password: "Password12!",
      password_confirm: "Password12!",
    };

    cy.visit(`/password-reset/${params.password_id}`);
    cy.findByTestId("password").type(params.new_password);
    cy.findByTestId("password_confirm").type(params.password_confirm);
    cy.findByTestId("submit").should("not.be.disabled").click();

    cy.wait("@changePwApi").its("request.body").should("deep.equal", params);
    cy.findByText("Password has been successfully updated.");
    cy.findByTestId("password").should("be.value", "");
    cy.findByTestId("password_confirm").should("be.value", "");
  });
});

describe("Password Reset page for authenticated user", () => {
  it.only("successfully load", () => {
    cy.setAsLoggedIn();
    cy.visit("/password-reset");
    cy.contains("h1", "Change Password");
    cy.findByTestId("submit").should("be.disabled");
  });

  const tests: { name: string; msg: string; res: {}; visit: Function }[] = [
    {
      name: "displays confrimation message",
      msg: "Password has been successfully updated.",
      res: { ok: "password changed" },
      visit: () => cy.visit("/password-reset"),
    },
    {
      name: "displays confrimation message when visiting with password-id",
      msg: "Password has been successfully updated.",
      res: { ok: "password changed" },
      visit: () => cy.visit("/password-reset/fake-id"),
    },
    {
      name: "displays error message",
      msg: "We were unable to process your request, please try again.",
      res: {
        statusCode: 400,
        body: { error: "reset for account does not exist" },
      },
      visit: () => cy.visit("/password-reset"),
    },
    {
      name: "displays error message when visiting with password-id",
      msg: "We were unable to process your request, please try again.",
      res: {
        statusCode: 400,
        body: { error: "reset for account does not exist" },
      },
      visit: () => cy.visit("/password-reset/fake-id"),
    },
  ];
  tests.forEach(({ name, msg, res, visit }) => {
    it(name, () => {
      cy.intercept(
        {
          method: "POST",
          url: "/change_password",
        },
        res
      ).as("changePwApi");

      cy.setAsLoggedIn();
      visit();

      const params: { [key: string]: string } = {
        new_password: "Password12!",
        password_confirm: "Password12!",
      };

      cy.findByTestId("password").type(params.new_password);
      cy.findByTestId("password_confirm").type(params.password_confirm);
      cy.findByTestId("submit").should("not.be.disabled").click();

      cy.wait("@changePwApi").its("request.body").should("deep.equal", params);
      cy.findByText(msg);
      cy.findByTestId("password").should("be.empty");
      cy.findByTestId("password").should("be.empty");
    });
  });
});
