import type { RouteMatcherOptions } from "cypress/types/net-stubbing";

export function stubCheckTotpApi(
  {
    route,
    response,
  }: {
    route?: RouteMatcherOptions;
    response?: object;
  } = {
    route: {},
    response: {},
  }
) {
  cy.intercept(
    {
      method: "POST",
      url: "/check_totp",
      ...route,
    },
    {
      body: {
        ok: {
          type: "basic",
          refresh_token: "refresh_token",
          refresh_expire_in: 0,
          access_token: "access_token",
          access_expire_in: 300,
        },
      },
      ...response,
    }
  ).as("totpApi");
}

export function stubEnable2faApi(
  {
    route,
    response,
  }: {
    route?: RouteMatcherOptions;
    response?: object;
  } = {
    route: {},
    response: {},
  }
) {
  cy.intercept(
    {
      method: "POST",
      url: "/enable_2fa",
      ...route,
    },
    {
      body: {
        ok: "two factor auth enabled",
      },
      ...response,
    }
  ).as("stubEnable2faApi");
}

export function stubDisable2faApi(
  {
    route,
    response,
  }: {
    route?: RouteMatcherOptions;
    response?: object;
  } = {
    route: {},
    response: {},
  }
) {
  cy.intercept(
    {
      method: "POST",
      url: "/disable_2fa",
      ...route,
    },
    {
      body: {
        ok: "two factor auth disabled",
      },
      ...response,
    }
  ).as("stubDisable2faApi");
}

export function stubTotpUrlApi(
  {
    route,
    response,
  }: {
    route?: RouteMatcherOptions;
    response?: object;
  } = {
    route: {},
    response: {},
  }
) {
  cy.intercept(
    {
      method: "POST",
      url: "/generate_totp_url",
      ...route,
    },
    {
      body: {
        ok: "otpauth://totp/Acme:alice?secret=TESTFAKE&issuer=Acme",
      },
      ...response,
    }
  ).as("stubTotpUrlApi");
}

export function stubRecoveryCodeApi(
  {
    route,
    response,
  }: {
    route?: RouteMatcherOptions;
    response?: object;
  } = {
    route: {},
    response: {},
  }
) {
  cy.intercept(
    {
      method: "POST",
      url: "/generate_recovery_codes",
      ...route,
    },
    {
      body: {
        ok: [
          "099345",
          "076698",
          "115899",
          "327675",
          "951647",
          "191725",
          "333458",
          "715267",
          "565345",
          "010398",
        ],
      },
      ...response,
    }
  ).as("stubRecoveryCodeApi");
}
