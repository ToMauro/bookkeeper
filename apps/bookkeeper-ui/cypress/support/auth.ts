import type { RouteMatcherOptions } from "cypress/types/net-stubbing";

export function stubCheckApi(
  {
    route,
    response,
  }: {
    route?: RouteMatcherOptions;
    response?: object;
  } = {
    route: {},
    response: {
      body: {
        ok: {
          username: "testuser123",
          meta: {},
          id: 1,
          email_confirmed: true,
          email: "testuser123@gmail.com",
        },
      },
    },
  }
) {
  cy.intercept(
    {
      method: "GET",
      url: "/check",
      ...route,
    },
    {
      ...response,
    }
  ).as("stubCheckApi");
}

export function setAsLoggedIn(enable2fa = false) {
  cy.visit("/");
  cy.window().should("have.property", "$nuxt");

  cy.window().then((window) => {
    cy.stubCheckApi({
      response: {
        body: {
          ok: {
            username: "testuser123",
            enable_2fa: enable2fa,
            meta: {},
            id: 1,
            email_confirmed: true,
            email: "testuser123@gmail.com",
          },
        },
      },
    });
    window.$nuxt.$auth.setUserToken("fake-token", "fake-refresh-token");
    cy.wait("@stubCheckApi");
  });
}
