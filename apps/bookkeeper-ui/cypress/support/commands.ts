/// <reference types="cypress" />
// ***********************************************
// This example commands.ts shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
//
// declare global {
//   namespace Cypress {
//     interface Chainable {
//       login(email: string, password: string): Chainable<void>
//       drag(subject: string, options?: Partial<TypeOptions>): Chainable<Element>
//       dismiss(subject: string, options?: Partial<TypeOptions>): Chainable<Element>
//       visit(originalFn: CommandOriginalFn, url: string, options: Partial<VisitOptions>): Chainable<Element>
//     }
//   }
// }

import "@testing-library/cypress/add-commands";
import type { RouteMatcherOptions } from "cypress/types/net-stubbing";
import { setAsLoggedIn, stubCheckApi } from "./auth";
import {
  stubCheckTotpApi,
  stubEnable2faApi,
  stubDisable2faApi,
  stubRecoveryCodeApi,
  stubTotpUrlApi,
} from "./api/2fa";

declare global {
  namespace Cypress {
    interface Chainable {
      stubCheckTotpApi(overrides?: {
        route?: RouteMatcherOptions;
        response?: object;
      }): Chainable<null>;
      stubEnable2faApi(overrides?: {
        route?: RouteMatcherOptions;
        response?: object;
      }): Chainable<null>;
      stubDisable2faApi(overrides?: {
        route?: RouteMatcherOptions;
        response?: object;
      }): Chainable<null>;
      setAsLoggedIn(enable_2fa?: boolean): Chainable<void>;
      stubCheckApi(overrides?: {
        route?: RouteMatcherOptions;
        response?: object;
      }): Chainable<null>;
      stubRecoveryCodeApi(overrides?: {
        route?: RouteMatcherOptions;
        response?: object;
      }): Chainable<null>;
      stubTotpUrlApi(overrides?: {
        route?: RouteMatcherOptions;
        response?: object;
      }): Chainable<null>;
    }
  }
}

Cypress.Commands.add("stubCheckTotpApi", stubCheckTotpApi);
Cypress.Commands.add("stubEnable2faApi", stubEnable2faApi);
Cypress.Commands.add("stubDisable2faApi", stubDisable2faApi);
Cypress.Commands.add("setAsLoggedIn", setAsLoggedIn);
Cypress.Commands.add("stubCheckApi", stubCheckApi);
Cypress.Commands.add("stubRecoveryCodeApi", stubRecoveryCodeApi);
Cypress.Commands.add("stubTotpUrlApi", stubTotpUrlApi);
