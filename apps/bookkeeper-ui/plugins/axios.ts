import { defineNuxtPlugin } from "@nuxtjs/composition-api";

export default defineNuxtPlugin(({ $axios }) => {
  $axios.onRequest((config) => {
    if (config.url === "/refresh") {
      config.headers["refresh-token"] = config.data.refresh_token;
      config.data = "";
    }

    return config;
  });

  $axios.onResponse((response) => {
    if (response.config.url === "/refresh") {
      response.data.ok = { access_token: response.data.ok };
    }

    return response;
  });
});
