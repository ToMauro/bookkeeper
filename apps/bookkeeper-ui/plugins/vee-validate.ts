import { extend } from "vee-validate";
import {
  alpha_dash as alphaDash,
  alpha_num as alphaNum,
  confirmed,
  digits,
  email,
  min,
  required,
} from "vee-validate/dist/rules";

const re = /!|@|#|\$|%|\^|&|\*/;
const reNum = /\d/;
const reCapital = /[A-Z]/;

extend("alpha_dash", {
  ...alphaDash,
  message:
    "{_field_} may ONLY contain alphabetic characters, numbers, dashes or underscores.",
});
extend("alpha_special", {
  validate: (value: string) => {
    const isAlphaNum = alphaNum.validate(value);
    return isAlphaNum || re.test(value);
  },
  message:
    "{_field_} may ONLY contain alphabetic characters, numbers, or !@#$%^&*.",
});
extend("confirmed", {
  ...confirmed,
  message: "{_field_} does not match {target}.",
});
extend("digits", {
  ...digits,
  message:
    "{_field_} needs to be numeric and have a length of {length} characters.",
});
extend("email", {
  ...email,
  message: "Provided input is not a valid email.",
});
extend("min", {
  ...min,
  message: "{_field_} need minimum {length} characters.",
});
extend("min_num", {
  validate: (value: string) => reNum.test(value),
  message: "{_field_} requires at least one number character.",
});
extend("min_special", {
  validate: (value: string) => re.test(value),
  message:
    "{_field_} requires at least one special character(example: !@#$%^&*).",
});
extend("min_uppercase", {
  validate: (value: string) => reCapital.test(value),
  message: "{_field_} requires at least one uppercase character.",
});
extend("required", {
  ...required,
  message: "{_field_} is a required.",
});
