import { userEvent, within } from "@storybook/testing-library";
import { expect } from "@storybook/jest";

export default {
  title: "pages / login",
};

const Template = (_args, { argTypes }) => ({
  props: Object.keys(argTypes),
  template: "<auth-login />",
});

export const Default = Template.bind({});

export const RequiredError = Template.bind({});
RequiredError.play = async ({ _canvasElement }) => {
  const canvasElement = document.querySelector("#root"); // work around
  const canvas = within(canvasElement);

  let el = canvas.getByTestId("username");
  el.focus();
  el.blur();

  el = canvas.getByTestId("password");
  el.focus();
  el.blur();

  let error = await canvas.findByText("Username is a required.");
  await expect(error).toBeInTheDocument();
  error = await canvas.findByText("Password is a required.");
  await expect(error).toBeInTheDocument();
};

export const Filled = Template.bind({});
Filled.play = async ({ _canvasElement }) => {
  const canvasElement = document.querySelector("#root"); // work around
  const canvas = within(canvasElement);
  let el = canvas.getByTestId("username");
  await userEvent.type(el, "test_account");

  el = canvas.getByTestId("password");
  await userEvent.type(el, "Password1!");
};
