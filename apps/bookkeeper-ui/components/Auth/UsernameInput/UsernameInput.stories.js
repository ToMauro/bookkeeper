import { within } from "@storybook/testing-library";
import { expect } from "@storybook/jest";

export default {
  title: "auth / unsername-input",
  argTypes: {
    value: {
      control: "text",
      description: "Username input",
    },
  },
};

const Template = (_args, { argTypes }) => ({
  props: Object.keys(argTypes),
  template: '<auth-username-input v-bind="$props"/>',
});

export const Default = Template.bind({});
Default.args = { value: "" };

export const RequiredError = Template.bind({});
RequiredError.args = { value: "" };
RequiredError.play = async ({ _canvasElement }) => {
  const canvasElement = document.querySelector("#root"); // work around
  const canvas = within(canvasElement);

  const el = canvas.getByTestId("username");
  el.focus();
  el.blur();

  const error = await canvas.findByText("Username is a required.");
  await expect(error).toBeInTheDocument();
};

export const InputError = Template.bind({});
InputError.args = { value: "   invalid username" };
InputError.play = async ({ _canvasElement }) => {
  const canvasElement = document.querySelector("#root"); // work around
  const canvas = within(canvasElement);

  const el = canvas.getByTestId("username");
  el.focus();
  el.blur();

  const error = await canvas.findByText(
    "Username may ONLY contain alphabetic characters, numbers, dashes or underscores."
  );
  await expect(error).toBeInTheDocument();
};

export const LengthError = Template.bind({});
LengthError.args = { value: "s" };
LengthError.play = async ({ _canvasElement }) => {
  const canvasElement = document.querySelector("#root"); // work around
  const canvas = within(canvasElement);

  const el = canvas.getByTestId("username");
  el.focus();
  el.blur();

  const error = await canvas.findByText("Username need minimum 5 characters.");
  await expect(error).toBeInTheDocument();
};
