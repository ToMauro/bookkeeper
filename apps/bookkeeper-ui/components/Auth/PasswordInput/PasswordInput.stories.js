import { within } from "@storybook/testing-library";
import { expect } from "@storybook/jest";

export default {
  title: "auth / password-input",
  argTypes: {
    password: {
      control: "text",
      description: "Password input",
    },
    passwordConfirm: {
      control: "text",
      description: "Password confirmation input",
    },
  },
};

const Template = (_args, { argTypes }) => ({
  props: Object.keys(argTypes),
  template: '<auth-password-input v-bind="$props"/>',
});
Template.args = { password: "", passwordConfirm: "" };

export const Default = Template.bind({});
Default.args = { ...Template.args, password: "Password1!" };

export const RequiredError = Template.bind({});
RequiredError.args = { ...Template.args };
RequiredError.play = async ({ _canvasElement }) => {
  const canvasElement = document.querySelector("#root"); // work around
  const canvas = within(canvasElement);

  let el = canvas.getByTestId("password");
  el.focus();
  el.blur();

  el = canvas.getByTestId("password_confirm");
  el.focus();
  el.blur();

  let error = await canvas.findByText("Password is a required.");
  await expect(error).toBeInTheDocument();
  error = await canvas.findByText("Confirm Password is a required.");
  await expect(error).toBeInTheDocument();
};

export const InputError = Template.bind({});
InputError.args = { ...Template.args, password: "   invalid password" };
InputError.play = async ({ _canvasElement }) => {
  const canvasElement = document.querySelector("#root"); // work around
  const canvas = within(canvasElement);

  const el = canvas.getByTestId("password");
  el.focus();
  el.blur();

  const error = await canvas.findByText(
    "Password may ONLY contain alphabetic characters, numbers, or !@#$%^&*."
  );
  await expect(error).toBeInTheDocument();
};

export const LengthError = Template.bind({});
LengthError.args = { ...Template.args, password: "s" };
LengthError.play = async ({ _canvasElement }) => {
  const canvasElement = document.querySelector("#root"); // work around
  const canvas = within(canvasElement);

  const el = canvas.getByTestId("password");
  el.focus();
  el.blur();

  const error = await canvas.findByText("Password need minimum 8 characters.");
  await expect(error).toBeInTheDocument();
};

export const NumberCharError = Template.bind({});
NumberCharError.args = { ...Template.args, password: "Password!" };
NumberCharError.play = async ({ _canvasElement }) => {
  const canvasElement = document.querySelector("#root"); // work around
  const canvas = within(canvasElement);

  const el = canvas.getByTestId("password");
  el.focus();
  el.blur();

  const error = await canvas.findByText(
    "Password requires at least one number character."
  );
  await expect(error).toBeInTheDocument();
};

export const SpecialCharError = Template.bind({});
SpecialCharError.args = { ...Template.args, password: "Password1" };
SpecialCharError.play = async ({ _canvasElement }) => {
  const canvasElement = document.querySelector("#root"); // work around
  const canvas = within(canvasElement);

  const el = canvas.getByTestId("password");
  el.focus();
  el.blur();

  const error = await canvas.findByText(
    "Password requires at least one special character(example: !@#$%^&*)."
  );
  await expect(error).toBeInTheDocument();
};

export const UppercaseCharError = Template.bind({});
UppercaseCharError.args = { ...Template.args, password: "password1!" };
UppercaseCharError.play = async ({ _canvasElement }) => {
  const canvasElement = document.querySelector("#root"); // work around
  const canvas = within(canvasElement);

  const el = canvas.getByTestId("password");
  el.focus();
  el.blur();

  const error = await canvas.findByText(
    "Password requires at least one uppercase character."
  );
  await expect(error).toBeInTheDocument();
};

export const ConfirmationError = Template.bind({});
ConfirmationError.args = {
  password: "password1!",
  passwordConfirm: "password1",
};
ConfirmationError.play = async ({ _canvasElement }) => {
  const canvasElement = document.querySelector("#root"); // work around
  const canvas = within(canvasElement);

  const el = canvas.getByTestId("password_confirm");
  el.focus();
  el.blur();

  const error = await canvas.findByText(
    "Confirm Password does not match Password."
  );
  await expect(error).toBeInTheDocument();
};
