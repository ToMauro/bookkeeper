import { userEvent, within } from "@storybook/testing-library";
import { expect } from "@storybook/jest";

export default {
  title: "pages / forgot-username",
};

const Template = (_args, { argTypes }) => ({
  props: Object.keys(argTypes),
  template: "<auth-forgot-username />",
});

export const Default = Template.bind({});

export const RequiredError = Template.bind({});
RequiredError.play = async ({ _canvasElement }) => {
  const canvasElement = document.querySelector("#root"); // work around
  const canvas = within(canvasElement);

  const el = canvas.getByTestId("email");
  el.focus();
  el.blur();

  const error = await canvas.findByText("Email is a required.");
  await expect(error).toBeInTheDocument();
};

export const Filled = Template.bind({});
Filled.play = async ({ _canvasElement }) => {
  const canvasElement = document.querySelector("#root"); // work around
  const canvas = within(canvasElement);

  const el = canvas.getByTestId("email");
  await userEvent.type(el, "test_account@gmail.com");
};
