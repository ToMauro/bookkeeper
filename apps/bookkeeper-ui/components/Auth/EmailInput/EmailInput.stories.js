import { within } from "@storybook/testing-library";
import { expect } from "@storybook/jest";

export default {
  title: "auth / email-input",
  argTypes: {
    value: {
      control: "text",
      description: "Email input",
    },
  },
};

const Template = (_args, { argTypes }) => ({
  props: Object.keys(argTypes),
  template: '<auth-email-input v-bind="$props"/>',
});

export const Default = Template.bind({});
Default.args = { value: "" };

export const RequiredError = Template.bind({});
RequiredError.args = { value: "" };
RequiredError.play = async ({ _canvasElement }) => {
  const canvasElement = document.querySelector("#root"); // work around
  const canvas = within(canvasElement);

  const el = canvas.getByTestId("email");
  el.focus();
  el.blur();

  const error = await canvas.findByText("Email is a required.");
  await expect(error).toBeInTheDocument();
};

export const InputError = Template.bind({});
InputError.args = { value: "not an email" };
InputError.play = async ({ _canvasElement }) => {
  const canvasElement = document.querySelector("#root"); // work around
  const canvas = within(canvasElement);

  const el = canvas.getByTestId("email");
  el.focus();
  el.blur();

  const error = await canvas.findByText("Provided input is not a valid email.");
  await expect(error).toBeInTheDocument();
};
