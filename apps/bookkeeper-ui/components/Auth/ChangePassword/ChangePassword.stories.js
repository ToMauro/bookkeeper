import { userEvent, within } from "@storybook/testing-library";

export default {
  title: "pages / change-password",
  argTypes: {
    passwordId: {
      control: "text",
      description: "Password reset id",
    },
  },
};

const Template = (_args, { argTypes }) => ({
  props: Object.keys(argTypes),
  template: `<auth-change-password v-bind="$props" />`,
});

export const Default = Template.bind({});
Default.args = {
  passwordId: "",
};

export const Filled = Template.bind({});
Filled.play = async ({ _canvasElement }) => {
  const canvasElement = document.querySelector("#root"); // work around
  const canvas = within(canvasElement);

  const password = "Password1!";
  let el = canvas.getByTestId("password");
  await userEvent.type(el, password);

  el = canvas.getByTestId("password_confirm");
  await userEvent.type(el, password);
};
