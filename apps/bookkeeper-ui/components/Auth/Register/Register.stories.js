import { userEvent, within } from "@storybook/testing-library";

export default {
  title: "pages / register",
};

const Template = (_args, { argTypes }) => ({
  props: Object.keys(argTypes),
  template: "<auth-register />",
});

export const Default = Template.bind({});

export const Filled = Template.bind({});
Filled.play = async ({ _canvasElement }) => {
  const canvasElement = document.querySelector("#root"); // work around
  const canvas = within(canvasElement);
  let el = canvas.getByTestId("username");
  await userEvent.type(el, "test_account");

  el = canvas.getByTestId("email");
  await userEvent.type(el, "test_account@email.com");

  el = canvas.getByTestId("password");
  await userEvent.type(el, "Password1!");

  el = canvas.getByTestId("password_confirm");
  await userEvent.type(el, "Password1!");
};
