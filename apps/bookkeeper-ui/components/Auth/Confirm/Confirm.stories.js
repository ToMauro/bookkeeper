export default {
  title: "pages / confirm",
  argTypes: {
    isError: {
      control: "boolean",
      description: "Confirm api has errored",
    },
    isSuccess: {
      control: "boolean",
      description: "Confirm api has succeed",
    },
    loggedIn: {
      control: "boolean",
      description: "Confirm api has succeed",
    },
  },
};

const Template = (_args, { argTypes }) => ({
  props: Object.keys(argTypes),
  template: `<auth-confirm v-bind="$props" />`,
});

export const Default = Template.bind({});
Default.args = {
  isError: false,
  isSuccess: true,
  loggedIn: true,
};

export const LoggedOut = Template.bind({});
LoggedOut.args = { ...Default.args, loggedIn: false };

export const Error = Template.bind({});
Error.args = { ...Default.args, isError: true, isSuccess: false };

export const LoggedOutError = Template.bind({});
LoggedOutError.args = { isError: true, isSuccess: false, loggedIn: false };
