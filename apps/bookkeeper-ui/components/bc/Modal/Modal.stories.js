import { userEvent, within } from "@storybook/testing-library";
import { expect } from "@storybook/jest";

export default {
  title: "base components / bc-modal",
};

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  template: `
    <bc-modal v-bind="$props">
      <template v-if="${"header" in args}" #header>
        ${args.header}
      </template>
      ${args.default}
      <template v-if="${"footer" in args}" #footer>
        ${args.footer}
      </template>
    </bc-modal>`,
  mounted() {
    this.$modal.show("BcModal");
  },
});

export const Default = Template.bind({});
Default.args = {
  default: `
    <p>
      <b>Important</b>: Please make sure you save the recovery codes. Otherwise you can permanently lose access to your account if you lose your two-factor authentication device.
      <br/>
      If you lose your two-factor-enabled device, these codes can be used instead of the 6-digit two-factor authentication code to log into your account. Each code can only be used once.
    </p>

    <ul class="mt-4 text-center">
      <li>234dhghs-23nsdgnsdj</li>
    </ul>
  `,
};

export const WithHeader = Template.bind({});
WithHeader.args = {
  header: "Two-factor Authentication Recovery Codes",
  ...Default.args,
};

export const WithFooter = Template.bind({});
WithFooter.args = {
  footer: `
  <bc-button outline>Cancel</bc-button>
  <bc-button>Submit</bc-button>
  `,
  ...Default.args,
};

export const Closed = Template.bind({});
Closed.args = {
  ...Default.args,
};
Closed.play = async ({ _canvasElement }) => {
  const canvasElement = document.querySelector("#root"); // work around
  const canvas = within(canvasElement);

  const el = canvas.getByTestId("xmark");
  await userEvent.click(el);
  expect(el).not.toBeVisible();
};
