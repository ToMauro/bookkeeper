import { userEvent, within, waitFor } from "@storybook/testing-library";
import { expect } from "@storybook/jest";

export default {
  title: "base components / bc-navbar",
  argTypes: {
    loggedIn: {
      control: "boolean",
      description: "User's authenication status",
    },
  },
};

const Template = (_args, { argTypes }) => ({
  props: Object.keys(argTypes),
  template: `<bc-navbar v-bind="$props" />`,
});

export const Default = Template.bind({});
Default.args = { loggedIn: false };
Default.play = ({ _canvasElement }) => {
  const canvasElement = document.querySelector("#root"); // work around
  const canvas = within(canvasElement);

  canvas.findByText("Register");
  canvas.findByText("Login");
};

export const Authenticated = Template.bind({});
Authenticated.args = { loggedIn: true };
Authenticated.play = async ({ _canvasElement }) => {
  const canvasElement = document.querySelector("#root"); // work around
  const canvas = within(canvasElement);
  canvas.findByText("Shop Subscriptions");

  const el = canvas.getByTestId("navDropdownBtn");
  await userEvent.click(el);
  await waitFor(() => {
    expect(canvas.getByTestId("navDropdownBody")).toBeVisible();

    canvas.findByText("Signed in as");
    canvas.findByText("Change Password");
    canvas.findByText("Logout");
  });

  await userEvent.click(el);
  await waitFor(() => {
    expect(canvas.getByTestId("navDropdownBody")).not.toBeVisible();
  });
};
