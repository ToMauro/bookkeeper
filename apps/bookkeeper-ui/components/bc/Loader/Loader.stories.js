export default {
  title: "base components / bc-loader",
};

const Template = (_args, { argTypes }) => ({
  props: Object.keys(argTypes),
  template: `<bc-loader v-bind="$props" />`,
});

export const Default = Template.bind({});
