import { userEvent, within } from "@storybook/testing-library";
import { expect } from "@storybook/jest";

export default {
  title: "mfa / confirm-modal",
};

const Template = (_args, { argTypes }) => ({
  props: Object.keys(argTypes),
  template: '<mfa-confirm-modal v-bind="$props" />',
  mounted() {
    this.$modal.show("confirmModal");
  },
});

export const Default = Template.bind({});

export const Closed = Template.bind({});
Closed.play = async ({ _canvasElement }) => {
  const canvasElement = document.querySelector("#root"); // work around
  const canvas = within(canvasElement);

  const el = canvas.getByTestId("cancelBtn");
  await userEvent.click(el);
  expect(el).not.toBeVisible();
};
