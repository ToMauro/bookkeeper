import { userEvent, within } from "@storybook/testing-library";

export default {
  title: "mfa / disable-form",
};

const Template = (_args, { argTypes }) => ({
  props: Object.keys(argTypes),
  template: '<mfa-disable-form v-bind="$props" />',
});

export const Default = Template.bind({});

export const WithModal = Template.bind({});
WithModal.play = async ({ _canvasElement }) => {
  const canvasElement = document.querySelector("#root"); // work around
  const canvas = within(canvasElement);

  const el = canvas.getByTestId("submit");
  await userEvent.click(el);
};
