export default {
  title: "mfa / login-form",
};

const Template = (_args, { argTypes }) => ({
  props: Object.keys(argTypes),
  template: '<mfa-login-form v-bind="$props" />',
});

export const Default = Template.bind({});
Default.args = {
  data: {
    username: "testuser",
    password: "testpw",
  },
};
