import { userEvent, within } from "@storybook/testing-library";
import { expect } from "@storybook/jest";

export default {
  title: "mfa / recovery-code-modal",
};

const Template = (_args, { argTypes }) => ({
  props: Object.keys(argTypes),
  template: '<mfa-recovery-code-modal v-bind="$props" />',
  mounted() {
    this.$modal.show("recoveryCodeModal");
  },
});

export const Default = Template.bind({});
Default.args = {
  codes: ["sample-code-23424"],
};
export const Closed = Template.bind({});
Closed.args = {
  ...Default.args,
};

Closed.play = async ({ _canvasElement }) => {
  const canvasElement = document.querySelector("#root"); // work around
  const canvas = within(canvasElement);

  const el = canvas.getByTestId("closeBtn");
  await userEvent.click(el);
  expect(el).not.toBeVisible();
};
