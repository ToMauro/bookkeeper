export default {
  title: "mfa / enable-form",
};

const Template = (_args, { argTypes }) => ({
  props: Object.keys(argTypes),
  template: '<mfa-enable-form v-bind="$props" />',
});

export const Default = Template.bind({});
Default.args = {
  uri: "otpauth://totp/Acme:alice?secret=MFRGGZAA&issuer=Acme",
};
